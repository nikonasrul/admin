<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">John Doe</p>
          <p class="app-sidebar__user-designation">Frontend Developer</p>
        </div>
      </div>
      <ul class="app-menu">
      <li><a class="app-menu__item" href="/home"><i class="app-menu__icon fa fa-home fa-lg"></i><span class="app-menu__label">Home</span></a></li>
      <li><a class="app-menu__item" href="/article"><i class="app-menu__icon fa fa-th-list"></i><span class="app-menu__label">Artikel</span></a></li>
      <li><a class="app-menu__item" href="/project"><i class="app-menu__icon fa fa-laptop"></i><span class="app-menu__label">Project</span></a></li>
      <li><a class="app-menu__item" href="/about"><i class="app-menu__icon fa fa-question-circle" aria-hidden="true"></i><span class="app-menu__label">About</span></a></li>
      <li><a class="app-menu__item" href="/contact"><i class="app-menu__icon fa fa-phone" aria-hidden="true"></i></i><span class="app-menu__label">Contact Us</span></a></li>
      <li><a class="app-menu__item" href="/faq"><i class="app-menu__icon fa fa-bookmark" aria-hidden="true"></i><span class="app-menu__label">FAQ</span></a></li>
      </ul>
    </aside>
    </div>